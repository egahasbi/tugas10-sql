1.Membuat Database

CREATE DATABASE myshop;

2.Membuat Table di Dalam Database

//Table users
CREATE TABLE users(
	id int PRIMARY KEY AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	email varchar(255) NOT NULL,
	password varchar(255) NOT NULL
);

//Table categories
CREATE TABLE categories(
	id int PRIMARY KEY AUTO_INCREMENT,
	name varchar(255) NOT NULL
);

//Table items
CREATE TABLE items(
	id int PRIMARY KEY AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	description varchar(255) NOT NULL,
	price int NOT NULL,
	stock int NOT NULL,
	category_id int NOT NULL,
	FOREIGN KEY(category_id) REFERENCES categories(id)
);

3.Memasukkan Data pada Table

//insert data ke tabel users
INSERT INTO users(name,email,password) VALUES("John Doe","john@doe.com","john123"),("John Doe","john@doe.com","jenita123");

//inser data ke tabel categories
INSERT INTO categories(name) VALUES("gadget"),("cloth"),("men"),("women"),("branded");

//insert data ke tabel items
INSERT INTO items(name,description,price,stock,category_id) VALUES("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),("Uniklooh","baju keren dari brand ternama",500000,50,2),("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4.Mengambil Data dari Database

a. Mengambil data users kecuali password;
SELECT id,name,email FROM users;

b. Mengambil data items
//Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
SELECT * FROM items WHERE price > 1000000;

//Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
SELECT * FROM items WHERE name LIKE '%watch';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori FROM items INNER JOIN categories on items.category_id = categories.id;

5.Mengubah Data dari Database
UPDATE items set price=2500000 WHERE name='Sumsang b50'; 



